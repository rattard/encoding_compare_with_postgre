#!/usr/bin/python
import sys
import psycopg2
import zlib
import bson
import time
from hashlib import sha256
from optparse import OptionParser
from random import randint,random
NUM_TESTS = 2000

def setup():
    conn = psycopg2.connect("host='localhost' dbname='testdb' user='testuser' password='password'")
    cur = conn.cursor()
    cur.execute("DROP TABLE IF EXISTS test_list")
    cur.execute("DROP TABLE IF EXISTS test_dict")
    cur.execute("CREATE TABLE test_list (id serial PRIMARY KEY, data bytea)")
    cur.execute("CREATE TABLE test_dict (id serial PRIMARY KEY, data bytea)")
    return conn,cur

def dict_test(curr,conn):
    log = open('dict.log','w')
    raw_sizes_dict = []
    compressed_sizes_dict = []
    compressed_sizes_bson = []
    encoding_times_zlib = []
    encoding_times_bson = []
    decoding_times_zlib = []
    decoding_times_bson = []

    for i in range(1,NUM_TESTS):
        #Generation block simulating raw data
        randdict = {} 
        offset = random()
        index=0
        for j in range(100000):
            index += offset
            randdict[str(index)] = randint(0,255)
        uncompressed_dict_size = sys.getsizeof(randdict)
        raw_sizes_dict.append(uncompressed_dict_size)
        uncompressed_dict_sha256 = sha256(str(randdict)).hexdigest()
        #zlib block
            #Encoding block
        start = time.clock()
        compressed_dict = zlib.compress(str(randdict),1)
        zlib_encoding_time = time.clock() - start
        encoding_times_zlib.append(zlib_encoding_time)
        compressed_dict_size = sys.getsizeof(compressed_dict)
        compressed_sizes_dict.append(compressed_dict_size)
        compressed_dict_sha256 = sha256(compressed_dict).hexdigest()
        #Test insert into postgre
        cur.execute("INSERT INTO test_dict (data) VALUES (%s)",(psycopg2.Binary(compressed_dict),))
        cur.execute("SELECT * from test_dict where id = %s",(i,))
        result = cur.fetchone()
            #Decoding block
        start = time.clock()
        returned_compressed_dict = str(result[1])
        returned_decompressed_dict = zlib.decompress(returned_compressed_dict)
        decoding_time_zlib = time.clock() - start
        decoding_times_zlib.append(decoding_time_zlib)
        returned_compressed_sha256 = sha256(returned_compressed_dict).hexdigest()
        returned_compressed_size = sys.getsizeof(returned_compressed_dict)
        returned_decompressed_size = sys.getsizeof(eval(returned_decompressed_dict))
        returned_decompressed_sha256 = sha256(returned_decompressed_dict).hexdigest()
        #BSON block
            #Encoding
        start = time.clock()
        bson_string = bson.dumps(randdict)
        bson_encoding_time = time.clock() - start
        encoding_times_bson.append(bson_encoding_time)
        bson_size = sys.getsizeof(bson_string)
        compressed_sizes_bson.append(sys.getsizeof(bson_string))
            #Decoding
        start = time.clock()
        bson_raw = bson.loads(bson_string)
        bson_decoding_time = time.clock() - start
        decoding_times_bson.append(bson_encoding_time)
        
        log.write( "                     Index: %i\n"%(i))
        log.write( "             Original Size: %s sha256: %s\n"%(uncompressed_dict_size,uncompressed_dict_sha256))
        log.write( "Original Decompressed Size: %s sha256: %s\n"%(returned_decompressed_size,returned_decompressed_sha256))
        log.write( "      BSON Compressed Size: %s \n"%(bson_size))
        log.write( "           Compressed Size: %s sha256: %s\n"%(compressed_dict_size,compressed_dict_sha256))
        log.write( "  Returned Compressed Size: %s sha256: %s\n"%(returned_compressed_size,returned_compressed_sha256))
    log.close()
    cur.execute("SELECT pg_size_pretty(pg_total_relation_size('test_dict'))")
    table_size = (cur.fetchone()[0])
    print "Table size dict:%s"%table_size
    cur.execute("DROP TABLE IF EXISTS test_dict")
    compressed_sizes_dict_average = sum(compressed_sizes_dict) / float(len(compressed_sizes_dict)) 
    compressed_sizes_bson_average = sum(compressed_sizes_bson) / float(len(compressed_sizes_dict)) 
    raw_sizes_dict_average = sum(raw_sizes_dict) / float(len(raw_sizes_dict))
    compression_ratio_dict = raw_sizes_dict_average / compressed_sizes_dict_average 
    compression_ratio_bson = raw_sizes_dict_average / compressed_sizes_bson_average 
    encoding_time_bson = sum(encoding_times_bson) / float(len(encoding_times_bson))
    encoding_time_zlib = sum(encoding_times_zlib) / float(len(encoding_times_zlib))
    decoding_time_bson = sum(decoding_times_bson) / float(len(decoding_times_bson))
    decoding_time_zlib = sum(decoding_times_zlib) / float(len(decoding_times_zlib))
    retval = {"compressed_size_zlib":compressed_sizes_dict_average,"compressed_size_bson":compressed_sizes_bson_average,
              "raw_size_average":raw_sizes_dict_average,"compression_ratio_bson":compression_ratio_bson,
              "compression_ratio_zlib":compression_ratio_dict,"postgres_table_size":table_size,
              "encoding_time_zlib":encoding_time_zlib,"encoding_time_bson":encoding_time_bson,
              "decoding_time_zlib":decoding_time_zlib,"decoding_time_bson":decoding_time_bson}
    return retval

def list_test(conn,cur):
    log = open('list.log','w')
    raw_sizes_dict = []
    compressed_sizes_list = []
    raw_sizes_list = []
    encoding_times = []
    decoding_times = []
    for i in range(1,NUM_TESTS):
        #Generation block
        randlist = [] 
        for j in range(100000):
            randlist.append(randint(0,255))
        uncompressed_list_size = sys.getsizeof(randlist)
        uncompressed_list_sha256 = sha256(str(randlist)).hexdigest()
        #Encoding block
        start = time.clock()
        compressed_list = zlib.compress(str(randlist))
        encoding_time = time.clock() - start
        encoding_times.append(encoding_time)
        compressed_list_size = sys.getsizeof(compressed_list)
        compressed_list_sha256 = sha256(compressed_list).hexdigest()
        #Insertion block
        cur.execute("INSERT INTO test_list (data) VALUES (%s)",(psycopg2.Binary(compressed_list),))
        cur.execute("SELECT * from test_list where id = %s",(i,))
        result = cur.fetchone()
        start = time.clock()
        returned_compressed_list = str(result[1])
        returned_decompressed_list = zlib.decompress(returned_compressed_list)
        decoding_time = time.clock() - start
        decoding_times.append(encoding_time)
        returned_compressed_sha256 = sha256(returned_compressed_list).hexdigest()
        returned_compressed_size = sys.getsizeof(returned_compressed_list)
        returned_decompressed_size = sys.getsizeof(eval(returned_decompressed_list))
        returned_decompressed_sha256 = sha256(returned_decompressed_list).hexdigest()
        raw_sizes_list.append(uncompressed_list_size)
        compressed_sizes_list.append(compressed_list_size)
        log.write( "                     Index: %i\n"%(i))
        log.write( "             Original Size: %s sha256: %s\n"%(uncompressed_list_size,uncompressed_list_sha256))
        log.write( "Returned Decompressed Size: %s sha256: %s\n"%(returned_decompressed_size,returned_decompressed_sha256))
        log.write( "           Compressed Size: %s sha256: %s\n"%(compressed_list_size,compressed_list_sha256))
        log.write( "  Returned Compressed Size: %s sha256: %s\n"%(returned_compressed_size,returned_compressed_sha256))
    log.close()
    cur.execute("SELECT pg_size_pretty(pg_total_relation_size('test_list'))")
    table_size = cur.fetchone()[0]
    print "Table size list:%s"%table_size
    cur.execute("DROP TABLE IF EXISTS test_list")
    
    compressed_sizes_list_average = sum(compressed_sizes_list) / float(len(compressed_sizes_list))
    compressed_sizes_list_average = sum(compressed_sizes_list) / float(len(compressed_sizes_list))
    raw_sizes_list_average = sum(raw_sizes_list) / float(len(raw_sizes_list))
    compression_ratio_list = raw_sizes_list_average / compressed_sizes_list_average 
    encoding_time = sum(encoding_times) / float(len(encoding_times))
    decoding_time = sum(decoding_times) / float(len(decoding_times))
    retval = {"compressed_size_zlib":compressed_sizes_list_average,
              "raw_size_average":raw_sizes_list_average,
              "compression_ratio_zlib":compression_ratio_list,"postgres_table_size":table_size,
              "decoding_time":decoding_time,"encoding_time":encoding_time}
    return retval
if __name__ == '__main__':
    conn,cur = setup()    
    list_output = list_test(conn,cur)
    dict_output = dict_test(conn,cur)
    print "  Avg Raw size zlib/bson: %s"%(dict_output['raw_size_average'])
    print "       Avg Raw size list: %s"%(list_output['raw_size_average'])
    print "Avg Compressed size zlib: %s"%(dict_output['compressed_size_zlib'])
    print "Avg Compressed size bson: %s"%(dict_output['compressed_size_bson'])
    print "Avg Compressed size list: %s"%(list_output['compressed_size_zlib'])
    print "     Avg Compression ratio zlib: %s"%(dict_output['compression_ratio_zlib'])
    print "     Avg Compression ratio bson: %s"%(dict_output['compression_ratio_bson'])
    print "Avg Compression ratio zlib list: %s"%(list_output['compression_ratio_zlib'])
    print "     Average encoding time BSON: %s"%(dict_output['encoding_time_bson'])
    print "Average encoding time dict-zlib: %s"%(dict_output['encoding_time_zlib'])
    print "Average encoding time list-zlib: %s"%(list_output['encoding_time'])
    print "     Average decoding time BSON: %s"%(dict_output['decoding_time_bson'])
    print "Average decoding time dict-zlib: %s"%(dict_output['decoding_time_zlib'])
    print "Average decoding time list-zlib: %s"%(list_output['decoding_time'])

    cur.close()
    conn.close()
