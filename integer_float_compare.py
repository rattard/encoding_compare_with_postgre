import time
from random import random
def floating(max_iterations):
    time_elapsed = 0
    i=max_iterations
    while i > 0:
        i-=1
        val = float(random()*100000)
        val2 = float(random()*100000)
        start_time = time.clock()
        val * val2
        end_time = time.clock()
        time_elapsed += (end_time - start_time)
    return time_elapsed
def floating_square(max_iterations):
    time_elapsed = 0
    i=max_iterations
    while i > 0:
        i-=1
        val = float(random()*100000)
        start_time = time.clock()
        val * val
        end_time = time.clock()
        time_elapsed += (end_time - start_time)
    return time_elapsed


def fixed(max_iterations):
    time_elapsed = 0
    i=max_iterations
    while i > 0:
        i-=1
        val = int(random()*100000)
        val2 = int(random()*100000)
        start_time = time.clock()
        val * val2
        end_time = time.clock()
        time_elapsed += (end_time - start_time)
    return time_elapsed

def fixed_square(max_iterations):
    time_elapsed = 0
    i=max_iterations
    while i > 0:
        i-=1
        val = int(random()*100000)
        start_time = time.clock()
        val * val
        end_time = time.clock()
        time_elapsed += (end_time - start_time)
    return time_elapsed




if __name__ == '__main__':
    num_runs = 10000000
    print "For %s runs:"%(num_runs)
    print "----------------------------"
    print "Fixed squared time:%s"%(fixed_square(num_runs)/num_runs)
    print "Fixed arbitrary time:%s"%(fixed(num_runs)/num_runs)
    print "Floating squared time:%s"%(floating_square(num_runs)/num_runs)
    print "Floating time:%s"%(floating(num_runs)/num_runs)

